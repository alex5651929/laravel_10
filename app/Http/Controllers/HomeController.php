<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $blogs = [
            [
                'title' => 'Title one',
                'desc' => 'Description one',
                'status'=> 0,
            ],
            [
                'title' => 'Title two',
                'desc' => 'Description two',
                'status'=> 0,
            ],
            [
                'title' => 'Title thrid',
                'desc' => 'Description thrid',
                'status'=> 1,
            ],
            [
                'title' => 'Title for',
                'desc' => 'Description for',
                'status'=> 0,
            ],
        ];

        return view('home', compact('blogs'));
    }
}
