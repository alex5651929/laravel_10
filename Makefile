build:
	docker-compose up --build

up:
	docker-compose up -d

down:
	docker-compose down

bash-app:
	docker exec -it laravel_10_app_1 bash

bash-app-root:
	docker exec -u root -it laravel_10_app_1 bash
